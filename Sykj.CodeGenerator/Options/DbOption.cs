﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.CodeGenerator
{
    public class DbOption
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public DatabaseType DbType { get; set; }
    }
}
