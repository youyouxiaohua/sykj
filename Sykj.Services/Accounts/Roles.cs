﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sykj.Services
{
    /// <summary>
    /// 角色
    /// </summary>
    public class Roles : Sykj.Repository.RepositoryBase<Sykj.Entity.Roles>,Sykj.IServices.IRoles
    {
        public Roles(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }

        #region 组装树
        /// <summary>
        /// 组装树
        /// </summary>
        /// <returns></returns>
        public List<Sykj.ViewModel.ZTreeModel> GetTreeList()
        {
            List<Sykj.Entity.Roles> list = base.GetList().ToList();

            List<Sykj.ViewModel.ZTreeModel> treeList = new List<ViewModel.ZTreeModel>();
            if (list.Count > 0)
            {
                Sykj.ViewModel.ZTreeModel model;
                foreach (var item in list)
                {
                    model = new ViewModel.ZTreeModel();
                    model.id = item.RoleId;
                    model.name = item.Title;
                    model.open = true;
                    model.isParent = false;
                    treeList.Add(model);
                }
            }
            return treeList;
        }
        #endregion

        #region 根据角色ID获取权限列表
        /// <summary>
        /// 根据角色ID获取权限列表
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<Sykj.Entity.Permissions> GetPermissionsList(int roleId)
        {
            var list = from rolePermissions in _dbContext.Rolepermissions
                       join permissions in _dbContext.Permissions 
                       on rolePermissions.PermissionId equals permissions.PermissionId
                       where rolePermissions.RoleId == roleId
                       select permissions;
            return list.ToList();
        }
        #endregion

        #region 为角色分配权限
        /// <summary>
        /// 为角色分配权限
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool AddPermissions(List<Sykj.Entity.Rolepermissions> list)
        {
            if (!list.Any()) return false;

            var tran= _dbContext.Database.BeginTransaction();
            try
            {
                var roleId = list.First().RoleId;
                var oldRoldList = _dbContext.Rolepermissions.Where(c => c.RoleId == roleId);
                _dbContext.Rolepermissions.RemoveRange(oldRoldList);
                _dbContext.SaveChanges();

                _dbContext.Rolepermissions.AddRange(list);
                _dbContext.SaveChanges();

                tran.Commit();
                return true;
            }
            catch(Exception exc)
            {
                tran.Rollback();
                return false;
            }
        }
        #endregion

        #region 清空该角色下的所有权限
        /// <summary>
        /// 清空该角色下的所有权限
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public bool DeletePermissions(int roleId)
        {
             var oldRoldList = _dbContext.Rolepermissions.Where(c => c.RoleId == roleId);
             _dbContext.Rolepermissions.RemoveRange(oldRoldList);
            int row = _dbContext.SaveChanges();
            return (row > 0);
        }
        #endregion

        #region 删除角色
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public bool DeleteAll(List<int> ids)
        {
            var tran = _dbContext.Database.BeginTransaction();
            try
            {
                //删除该角色设置的权限
                var oldRoldList = _dbContext.Rolepermissions.Where(c => ids.Contains(c.RoleId));
                _dbContext.Rolepermissions.RemoveRange(oldRoldList);
                _dbContext.SaveChanges();

                //删除角色
                var list = GetList(c => ids.Contains(c.RoleId));
                Delete(list, false);
                _dbContext.SaveChanges();

                tran.Commit();
                return true;
            }
            catch (Exception exc)
            {
                tran.Rollback();
                return false;
            }
        }
        #endregion
    }
}
