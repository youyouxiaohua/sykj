﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Sykj.Services
{
    /// <summary>
    /// 用户对应角色
    /// </summary>
    public class UserRoles : Sykj.Repository.RepositoryBase<Sykj.Entity.UserRoles>,Sykj.IServices.IUserRoles
    {
        public UserRoles(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }
    }
}
