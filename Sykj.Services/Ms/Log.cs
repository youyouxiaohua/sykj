﻿

using Sykj.Repository;
using System.Data;

namespace Sykj.Services
{
    /// <summary>
    /// 日志
    /// </summary>
    public class Log : Sykj.Repository.RepositoryBase<Sykj.Entity.Log>,Sykj.IServices.ILog
    {
        public Log(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }
    }
}
