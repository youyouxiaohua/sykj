/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：字典表                                                    
*│　作    者：atz                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-03-21 09:09:01       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Services                                  
*│　类    名： Dic                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.Services
{
	/// <summary>
	/// 字典表
	/// </summary>
    public class Dic : Sykj.Repository.RepositoryBase<Sykj.Entity.Dic>,Sykj.IServices.IDic
    {
        public Dic(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {
		    
        }
    }
}