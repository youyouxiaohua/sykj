﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 广告位置
    /// </summary>
    public interface IAdvertiseposition : Sykj.Repository.IRepository<Sykj.Entity.Advertiseposition>
    {
        /// <summary>
        /// 删除广告位和其对应的广告内容
        /// </summary>
        /// <param name="ids">广告位id的集合</param>
        /// <returns></returns>
        bool Delete(List<int> ids);
    }
}
