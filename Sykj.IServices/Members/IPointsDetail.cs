/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：积分明细                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-06 14:26:00     
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.IServices                                   
*│　接口名称： IPointsdetail                                   
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
	/// 积分明细
	/// </summary>
    public interface IPointsDetail: Sykj.Repository.IRepository<Sykj.Entity.PointsDetail>
    {
	   
    }
}