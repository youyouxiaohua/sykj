/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：字典表                                                    
*│　作    者：atz                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-21 09:09:01 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Repository                                  
*│　类    名：Dic                                     
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 字典表
	/// </summary>
	public class DicMap : IEntityTypeConfiguration<Sykj.Entity.Dic>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Dic> entity)
        {
            entity.HasKey(e => e.Id);

            entity.ToTable("ms_dic");
        }
    }
}
