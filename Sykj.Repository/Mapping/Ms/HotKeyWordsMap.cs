/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-18 15:14:22 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Repository                                  
*│　类    名：Hotkeywords                                     
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 
	/// </summary>
	public class HotkeywordsMap : IEntityTypeConfiguration<Sykj.Entity.HotKeyWords>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.HotKeyWords> entity)
        {
            entity.HasKey(e => e.Id);

            entity.ToTable("ms_hotkeywords");
        }
    }
}
