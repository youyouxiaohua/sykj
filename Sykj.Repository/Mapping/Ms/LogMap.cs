﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class LogMap : IEntityTypeConfiguration<Sykj.Entity.Log>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Log> entity)
        {
            entity.HasKey(e => e.LogId);

            entity.ToTable("ms_log");
        }
    }
}
