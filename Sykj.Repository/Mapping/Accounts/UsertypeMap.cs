﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class UsertypeMap : IEntityTypeConfiguration<Sykj.Entity.Usertype>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Usertype> entity)
        {
            entity.HasKey(e => e.UserType);

            entity.ToTable("accounts_usertype");
        }
    }
}
