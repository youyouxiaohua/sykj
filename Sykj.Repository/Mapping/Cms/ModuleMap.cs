﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class ModuleMap : IEntityTypeConfiguration<Sykj.Entity.Module>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Module> entity)
        {
            entity.HasKey(e => e.ModuleId);

            entity.ToTable("cms_module");

        }
    }
}
