﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class AdvertisementMap : IEntityTypeConfiguration<Sykj.Entity.Advertisement>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Advertisement> entity)
        {
            entity.HasKey(e => e.AdvertisementId);

            entity.ToTable("ad_advertisement");
        }
    }
}
