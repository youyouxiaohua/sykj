﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sykj.IServices;
using System.Text;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class UsersController : MController
    {
        IUsers _users;
        IRoles _roles;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <param name="roles"></param>
        public UsersController(IUsers users, IRoles roles)
        {
            _users = users;
            _roles = roles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            //绑定角色列表
            ViewBag.RolesList = new MultiSelectList(_roles.GetList(), "RoleId", "Title");
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public IActionResult Add(Sykj.Entity.Users users)
        {
            bool flag = _users.IsExists(users.UserName);
            if (flag)
            {
                return Json(Failed("用户名已存在！"));
            }

            bool b = _users.AddRoles(users);
            Components.LogOperate.Add("添加账号", "添加ID为【" + users.UserId + "】的账号", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        public IActionResult List(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (UserName.Contains(@0) OR TrueName.Contains(@0))");
            }

            var list = _users.GetPagedListExt(page, limit, strWhere.ToString(), " UserId desc ", keyWords);
            int recordCount = _users.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Edit(int id)
        {
            //绑定角色列表
            ViewBag.RolesList = new MultiSelectList(_roles.GetList(), "RoleId", "Title");
            var model = _users.GetModel(c => c.UserId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            model.RoleIds = new List<int>();
            var roleList = _users.GetRoleList(model.UserId);
            if (roleList.Count > 0)
            {
                foreach (var item in roleList)
                {
                    model.RoleIds.Add(item.RoleId);
                }
            }
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Edit(Sykj.Entity.Users users)
        {
            var model = _users.GetModel(c => c.UserId == users.UserId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.TrueName = users.TrueName;
            model.Phone = users.Phone;
            model.Email = users.Email;
            model.Activity = users.Activity;
            model.RoleIds = users.RoleIds;
            bool b = _users.UpdateUserRole(model);
            Components.LogOperate.Add("编辑账号", "编辑ID为【" + users.UserId + "】的账号", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Delete(int id)
        {
            var model = _users.GetModel(c => c.UserId == id);
            if (model == null)
            {
                return Json(Failed("无效的数据"));
            }
            bool b = _users.Delete(model);
            Components.LogOperate.Add("删除账号", "删除ID为【" + id + "】的账号", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _users.GetList(c => ids.Contains(c.UserId));
            bool b = _users.Delete(list);
            Components.LogOperate.Add("批量删除账号", "删除ID为【" + string.Join(",", ids) + "】的账号", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 用户基本资料 ydp
        /// </summary>
        /// <returns></returns>
        public IActionResult Info()
        {
            ViewBag.RolesList = new MultiSelectList(_roles.GetList(), "RoleId", "Title");
            var model = _users.GetModel(c => c.UserId == UserId);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            model.RoleIds = new List<int>();
            var roleList = _users.GetRoleList(model.UserId);
            if (roleList.Count > 0)
            {
                foreach (var item in roleList)
                {
                    model.RoleIds.Add(item.RoleId);
                }
            }
            return View(model);
        }

        /// <summary>
        /// 修改基本信息 ydp
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Info(Sykj.Entity.Users users)
        {
            var model = _users.GetModel(c => c.UserId == UserId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.NickName = users.NickName;
            model.Gravatar = users.Gravatar;
            model.Sex = users.Sex;
            model.Phone = users.Phone;
            model.Email = users.Email;

            bool b = _users.Update<Sykj.Entity.Users>(model, true, x => x.NickName, x => x.Gravatar, x => x.Sex, x => x.Phone, x => x.Email);
            Components.LogOperate.Add("修改用户基本信息", "修改ID为【" + users.UserId + "】的账号", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 修改密码 ydp
        /// </summary>
        /// <returns></returns>
        public IActionResult UpdatePwd()
        {
            return View();
        }

        /// <summary>
        /// 修改密码 ydp
        /// </summary>
        /// <param name="currPass">当前密码</param>
        /// <param name="newPassword">新密码</param>
        /// <param name="reNewPassword">确认新密码</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UpdatePwd(string currPass, string newPassword, string reNewPassword)
        {
            var model = _users.GetModel(c => c.UserId == UserId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            var password = Infrastructure.DESEncrypt.Encrypt(currPass);
            if (model.Password != password)
            {
                return Json(Failed("当前密码错误，请重新输入！"));
            }
            if (newPassword != reNewPassword)
            {
                return Json(Failed("两次输入的新密码不一致，请重新核对！"));
            }
            model.Password = Infrastructure.DESEncrypt.Encrypt(reNewPassword);

            bool b = _users.Update<Sykj.Entity.Users>(model, true, x => x.Password);
           
            Components.LogOperate.Add("修改用户密码", "修改ID为【" + model.UserId + "】的账号", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 重置密码 bjg
        /// </summary>
        /// <param name="ids">批量重置密码</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ResetPwd(List<int> ids)
        {
            var list = _users.GetList(c => ids.Contains(c.UserId)).ToList();
            list.ForEach(x => x.Password = DESEncrypt.Encrypt("123456"));
            bool b = _users.UpdateRange(list);
            Components.LogOperate.Add("批量重置密码", "批量重置【" + string.Join(",", ids) + "】的密码", HttpContext);
            return Json(AjaxResult(b));
        }
    }
}