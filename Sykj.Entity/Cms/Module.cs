﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 模型
    /// </summary>
    public partial class Module
    {
        /// <summary>
        /// 模型ID
        /// </summary>
        public int ModuleId { get; set; }
        /// <summary>
        /// 模型类别名称
        /// </summary>
        public string TypeName { get; set; }
    }
}
