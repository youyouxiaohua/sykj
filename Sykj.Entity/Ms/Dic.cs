using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	/// <summary>
	/// 字典表
	/// </summary>
	public class Dic
	{
		/// <summary>
		/// 主键
		/// </summary>
		public Int32 Id {get;set;}

		/// <summary>
		/// 名称
		/// </summary>
		[MaxLength(100)]
        [Required(ErrorMessage = "请输入KEY！")]
        public String Name {get;set;}

		/// <summary>
		/// 父级ID
		/// </summary>
		public Int32? ParentId {get;set;}

		/// <summary>
		/// 内容
		/// </summary>
		[MaxLength(100)]
        [Required(ErrorMessage = "请输入VALUE！")]
        public String Value {get;set;}

		/// <summary>
		/// 业务分类（用于确定页面上的位置）
		/// </summary>
		public Int32? Types {get;set;}

		/// <summary>
		/// 创建日期
		/// </summary>
		public DateTime? CreateDate {get;set;}

		/// <summary>
		/// 创建人ID
		/// </summary>
		public Int32? UserId {get;set;}

		/// <summary>
		/// 备注
		/// </summary>
		[MaxLength(500)]
		public String Remark {get;set;}

        /// <summary>
        /// 标识
        /// </summary>
        [MaxLength(100)]
        [Required(ErrorMessage = "请输入标识！")]
        public String Signs { set; get; }
	}
}
