﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.Entity
{
    /// <summary>
    /// 缩略图
    /// </summary>
    public class ThumbnailSize
    {
        /// <summary>
        /// 缩略图编号
        /// </summary>
        public int ThumId { get; set; }

        /// <summary>
        /// 缩略图名称
        /// </summary>
        public string ThumName { get; set; }

        /// <summary>
        /// 缩略图宽度
        /// </summary>
        public int ThumWidth { get; set; }

        /// <summary>
        /// 缩略图高度
        /// </summary>
        public int ThumHeight { get; set; }

        /// <summary>
        /// 系统模块，0：shop 1:cms 2:sns 3:property 4:house
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 裁剪方式 HW:指定高宽缩放（补白）  W:指定宽，高按比例  H:指定高，宽按比例  Cut:指定高宽裁减（不变形）  Csize:调整图片大小
        /// </summary>
        public string ThumMode { get; set; }

        /// <summary>
        /// 是否为水印
        /// </summary>
        public bool IsWatermark { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public int CreateUserID { get; set; }
    }
}
