/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：lh                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-08 17:30:16 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Sitemessage                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	/// <summary>
	/// 
	/// </summary>
	public class SiteMessage
	{
		/// <summary>
		/// ID
		/// </summary>
		public Int32 Id {get;set;}

		/// <summary>
		/// 发送者 管理员为-1
		/// </summary>
		public Int32? SenderID {get;set;}

		/// <summary>
		/// 接收者
		/// </summary>
		public Int32? ReceiverID {get;set;}

		/// <summary>
		/// 标题
		/// </summary>
		[MaxLength(300)]
		public String Title {get;set;}

		/// <summary>
		/// 内容
		/// </summary>
		public String Content {get;set;}

		/// <summary>
		/// 类型 System:系统消息  Order:订单消息 Notice:通知消息
		/// </summary>
		[MaxLength(50)]
		public String MsgType {get;set;}

		/// <summary>
		/// 发送时间
		/// </summary>
		public DateTime SendTime {get;set;}

		/// <summary>
		/// 读取日期
		/// </summary>
		public DateTime? ReadTime {get;set;}

		/// <summary>
		/// 是否已读，false未读；true 已读
		/// </summary>
		public Boolean? ReceiverIsRead {get;set;}

		/// <summary>
		/// 发送者是否删除，false：未删除；true： 已删除
		/// </summary>
		public Boolean? SenderIsDel {get;set;}

		/// <summary>
		/// 接收者是否删除，false：未删除；true： 已删除
		/// </summary>
		public Boolean? ReaderIsDel {get;set;}

		/// <summary>
		/// 扩展1
		/// </summary>
		[MaxLength(300)]
		public String Ext1 {get;set;}

		/// <summary>
		/// 扩展2
		/// </summary>
		[MaxLength(300)]
		public String Ext2 {get;set;}
        
        /// <summary>
        /// 发送用户的用户名
        /// </summary>
        [NotMapped]
        public string SenderUser { get; set; }

        /// <summary>
        /// 接收用户的用户名
        /// </summary>
        [NotMapped]
        public string ReceivUser { get; set; }

        /// <summary>
        /// 接收用户的姓名
        /// </summary>
        [NotMapped]
        public string ReceivUserTrueName { get; set; }

        /// <summary>
        /// 接收者用户名的集合
        /// </summary>
        [NotMapped]
        public List<string> ReceiverUserNameList { get; set; } = new List<string>();
        
        /// <summary>
        /// 克隆
        /// </summary>
        /// <returns></returns>
        public SiteMessage Clone()
        {
            return this.MemberwiseClone() as SiteMessage;
        }
    }
}
