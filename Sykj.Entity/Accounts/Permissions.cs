﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 权限菜单
    /// </summary>
    public class Permissions
    {
        /// <summary>
        /// 权限ID
        /// </summary>
        public int PermissionId { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
        [Display(Name = "权限名称")]
        [Required(ErrorMessage = "此项不能为空")]
        public string Title { get; set; }
        /// <summary>
        /// 权限描述
        /// </summary>
        [Display(Name = "权限描述")]
        public string Description { get; set; }
        /// <summary>
        /// url
        /// </summary>
        [Display(Name = "url")]
        [Required(ErrorMessage = "此项不能为空")]
        public string Url { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Display(Name = "图标")]
        public string Icon { get; set; }
        /// <summary>
        /// 所属父级
        /// </summary>
        [Display(Name = "所属父级")]
        public int ParentId { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 深度
        /// </summary>
        public int Depth { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [Display(Name = "排序")]
        public int Sort { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Display(Name = "是否启用")]
        public bool IsEnable { get; set; }
        /// <summary>
        /// 类型：1:模块 2:菜单 3:按钮
        /// </summary>
        [Display(Name = "权限类型")]
        public int Type { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 所属角色ID，关联查询使用
        /// </summary>
        [NotMapped]
        public int RoleId { get; set; }
    }
}
