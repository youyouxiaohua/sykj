/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：用户账户信息                                                    
*│　作    者：liuxiang                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-01-21 16:44:28 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Users                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 用户账户信息
    /// </summary>
    public class Users
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public Int32 UserId { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "请输入用户名")]
        [MaxLength(100)]
        public String UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "请输入密码")]
        [MaxLength(100)]
        public String Password { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [MaxLength(100)]
        public String NickName { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [MaxLength(100)]
        public String TrueName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [MaxLength(50)]
        public String Sex { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        [MaxLength(100)]
        [RegularExpression(@"^(0|86|17951)?(13[0-9]|15[012356789]|17[013678]|18[0-9]|14[57])[0-9]{8}$", ErrorMessage = "请输入正确的手机号码")]
        public String Phone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(100)]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$", ErrorMessage = "请输入正确的邮箱地址")]
        public String Email { get; set; }

        /// <summary>
        /// 用户所在单位或部门ID
        /// </summary>
        [MaxLength(100)]
        public String DepartmentId { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        [Required(ErrorMessage = "请输入用户状态")]
        public bool Activity { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        [MaxLength(50)]
        public String UserType { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public Int32? Creator { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        [MaxLength(255)]
        public String Gravatar { get; set; }

        /// <summary>
        /// 微信OpenID
        /// </summary>
        [MaxLength(100)]
        public String WechatOpenId { get; set; }

        /// <summary>
        /// app应用OpenId
        /// </summary>
        [MaxLength(100)]
        public String AppOpenId { get; set; }

        /// <summary>
        /// 腾讯接口唯一标识
        /// </summary>
        [MaxLength(100)]
        public String UnionId { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        [Required(ErrorMessage = "请输入创建日期")]
        public DateTime CreateDate { get; set; }

        #region 扩展属性 ydp
        /// <summary>
        /// 所用角色ID集合
        /// </summary>
        [NotMapped]//不参与EF映射
        public List<int> RoleIds { get; set; } = new List<int>();

        /// <summary>
        /// 新密码
        /// </summary>
        [NotMapped]//不参与EF映射
        public string NewPassword { get; set; }
        #endregion

    }
}
