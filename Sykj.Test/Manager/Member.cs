﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 高校信息
    /// </summary>
    public class Member : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUsers _users;
        IUserMember _usersMember;
        IPointsDetail _pointsDetail;
        MemberController _memberController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Member()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _users = _serviceProvider.GetService<IUsers>();
            _usersMember = _serviceProvider.GetService<IUserMember>();
            _pointsDetail = _serviceProvider.GetService<IPointsDetail>();
            _memberController = new MemberController(_users, _usersMember, _pointsDetail);
        }

        /// <summary>
        /// 会员列表
        /// </summary>
        /// <param name="page">页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键词</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1, 10, "")]
        public void List(int page, int limit, string keyWords)
        {
            JsonResult r = _memberController.List(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 编辑性别
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sex"></param>
        /// <returns></returns>
        [Theory]
        [InlineData(124, "女")]
        public void SexEdit(int userId, string sex)
        {
            JsonResult r = _memberController.SexEdit(userId, sex) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 批量启用
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Fact]
        public void StartUsing()
        {
            List<int> list = new List<int>();
            list.Add(124);
            JsonResult r = _memberController.StartUsing(list) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 批量禁用
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Fact]
        public void Forbidden()
        {
            List<int> list = new List<int>();
            list.Add(124);
            JsonResult r = _memberController.Forbidden(list) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        [Theory]
        [InlineData(1, 10, "", null, null)]
        public void PointsDetailList(int page, int limit, string keyWords, DateTime start, DateTime end)
        {
            JsonResult r = _memberController.PointsDetailList(page, limit, keyWords, start, end) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        public void Dispose()
        {
        }
    }
}
