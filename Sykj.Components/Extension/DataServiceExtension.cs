﻿/*******************************************************************************
* Copyright (C) sykjwh.cn
* 
* Author: liuxiang
* Create Date: 2018/03/08 
* Description: Automated building by liuxiang20041986@qq.com 
* http://www.sykjwh.cn/
*********************************************************************************/
using Microsoft.Extensions.DependencyInjection;

namespace Sykj.Components
{
    /// <summary>
    /// 系统数据服务
    /// </summary>
    public static class DataServiceExtension
    {
        /// <summary>
        /// 注入数据
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddDataService(this IServiceCollection services)
        {
            #region 依赖注入
            services.AddScoped<Sykj.Repository.IUnitOfWork, Sykj.Repository.UnitOfWork>();
            services.AddScoped<Sykj.IServices.ILog, Sykj.Services.Log>();
            services.AddScoped<Sykj.IServices.IUsers,Sykj.Services.Users>();
            services.AddScoped<Sykj.IServices.IPermissions,Sykj.Services.Permissions>();
            services.AddScoped<Sykj.IServices.IRolepermissions,Sykj.Services.Rolepermissions>();
            services.AddScoped<Sykj.IServices.IUserRoles,Sykj.Services.UserRoles>();
            services.AddScoped<Sykj.IServices.IRoles,Sykj.Services.Roles>();
            services.AddScoped<Sykj.IServices.IUsertype,Sykj.Services.Usertype>();
            services.AddScoped<Sykj.IServices.IConfigsys,Sykj.Services.Configsys>();
            services.AddScoped<Sykj.IServices.IAdvertisement,Sykj.Services.Advertisement>();
            services.AddScoped<Sykj.IServices.IAdvertiseposition,Sykj.Services.Advertiseposition>();
            services.AddScoped<Sykj.IServices.IChannel,Sykj.Services.Channel>();
            services.AddScoped<Sykj.IServices.IContent,Sykj.Services.Content>();
            services.AddScoped<Sykj.IServices.IModule,Sykj.Services.Module>();
            services.AddScoped<Sykj.IServices.IImages,Sykj.Services.Images>();
            services.AddScoped<Sykj.IServices.IRegions, Sykj.Services.Regions>();
            services.AddScoped<Sykj.IServices.IUserMember, Sykj.Services.UserMember>();
            services.AddScoped<Sykj.IServices.IThumbnailSize, Sykj.Services.ThumbnailSize>();
            services.AddScoped<Sykj.IServices.IGuestbook, Sykj.Services.Guestbook>();
            services.AddScoped<Sykj.IServices.IFavorite, Sykj.Services.Favorite>();
            services.AddScoped<Sykj.IServices.IPointsDetail, Sykj.Services.PointsDetail>();
            services.AddScoped<Sykj.IServices.ISiteMessage, Sykj.Services.SiteMessage>();
            services.AddScoped<Sykj.IServices.IHotKeyWords, Sykj.Services.HotKeyWords>();
            services.AddScoped<Sykj.IServices.IUserinvite, Sykj.Services.Userinvite>();
            services.AddScoped<Sykj.IServices.IDic, Sykj.Services.Dic>();
            #endregion

            return services;
        }
    }
}
