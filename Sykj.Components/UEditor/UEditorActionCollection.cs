﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Sykj.Components
{
    /// <summary>
    /// action 处理类
    /// </summary>
    public class UEditorActionCollection:Dictionary<string,Action<HttpContext>>
    {
        public UEditorActionCollection()
        {
            Add("config", ConfigAction);
            Add("uploadimage", UploadImageAction);
            Add("uploadscrawl", UploadScrawlAction);
            Add("uploadvideo", UploadVideoAction);
            Add("uploadfile", UploadFileAction);
            Add("listimage", ListImageAction);
            Add("listfile", ListFileAction);
            Add("catchimage", CatchImageAction);
        }

        public new UEditorActionCollection Add(string action, Action<HttpContext> handler)
        {
            if (ContainsKey(action))
                this[action] = handler;
            else
                base.Add(action, handler);

            return this;
        }

        public new UEditorActionCollection Remove(string action)
        {
            base.Remove(action);
            return this;
        }

        /// <summary>
        /// 处理配置文件
        /// </summary>
        /// <param name="context"></param>
        private void ConfigAction(HttpContext context)
        {
            new ConfigHandler(context).Process();
        }

        /// <summary>
        /// 处理上传图片
        /// </summary>
        /// <param name="context"></param>
        private void UploadImageAction(HttpContext context)
        {
            new UploadHandler(context, new UploadConfig()
            {
                AllowExtensions = Config.GetStringList("imageAllowFiles"),
                PathFormat = Config.GetString("imagePathFormat"),
                SizeLimit = Config.GetInt("imageMaxSize"),
                UploadFieldName = Config.GetString("imageFieldName")
            }).Process();
        }

        /// <summary>
        /// 处理上传涂鸦
        /// </summary>
        /// <param name="context"></param>
        private void UploadScrawlAction(HttpContext context)
        {
            new UploadHandler(context, new UploadConfig()
            {
                AllowExtensions = new string[] { ".png" },
                PathFormat = Config.GetString("scrawlPathFormat"),
                SizeLimit = Config.GetInt("scrawlMaxSize"),
                UploadFieldName = Config.GetString("scrawlFieldName"),
                Base64 = true,
                Base64Filename = "scrawl.png"
            }).Process();
        }

        /// <summary>
        /// 处理上传视频
        /// </summary>
        /// <param name="context"></param>
        private void UploadVideoAction(HttpContext context)
        {
            new UploadHandler(context, new UploadConfig()
            {
                AllowExtensions = Config.GetStringList("videoAllowFiles"),
                PathFormat = Config.GetString("videoPathFormat"),
                SizeLimit = Config.GetInt("videoMaxSize"),
                UploadFieldName = Config.GetString("videoFieldName")
            }).Process();
        }

        /// <summary>
        /// 处理上传文件
        /// </summary>
        /// <param name="context"></param>
        private void UploadFileAction(HttpContext context)
        {
            new UploadHandler(context, new UploadConfig()
            {
                AllowExtensions = Config.GetStringList("fileAllowFiles"),
                PathFormat = Config.GetString("filePathFormat"),
                SizeLimit = Config.GetInt("fileMaxSize"),
                UploadFieldName = Config.GetString("fileFieldName")
            }).Process();
        }

        /// <summary>
        /// 处理图片列表请求
        /// </summary>
        /// <param name="context"></param>
        private void ListImageAction(HttpContext context)
        {
            new ListFileManager(
                    context,
                    Config.GetString("imageManagerListPath"),
                    Config.GetStringList("imageManagerAllowFiles"))
                .Process();
        }

        /// <summary>
        /// 处理文件列表请求
        /// </summary>
        /// <param name="context"></param>
        private void ListFileAction(HttpContext context)
        {
            new ListFileManager(
                    context,
                    Config.GetString("fileManagerListPath"),
                    Config.GetStringList("fileManagerAllowFiles"))
                .Process();
        }

        /// <summary>
        /// 处理涂鸦列表请求
        /// </summary>
        /// <param name="context"></param>
        private void CatchImageAction(HttpContext context)
        {
            new CrawlerHandler(context).Process();
        }
    }
}
