﻿/*******************************************************************************
* Copyright (C) sykjwh.cn
* 
* Author: liuxiang
* Create Date: 2018/03/08 
* Description: Automated building by liuxiang20041986@qq.com 
* http://www.sykjwh.cn/
*********************************************************************************/

namespace Sykj.Components
{
    /// <summary>
    /// 获取常量
    /// </summary>
    public class Constant
    {
        /// <summary>
        /// 图片服务器地址
        /// </summary>
        public const string IMAGESDOMAIN = "ImagesDomain";

        /// <summary>
        /// 主域名
        /// </summary>
        public const string DOMAIN = "BaseHost";

        /// <summary>
        /// API域名
        /// </summary>
        public const string APIDOMAIN = "ApiHost";

        /// <summary>
        /// 
        /// </summary>
        public const string WEBSITE = "WebSite";

        /// <summary>
        /// 管理后台区域名称
        /// </summary>
        public const string AREAMANAGER = "manager";

        /// <summary>
        /// 短信发送用户名
        /// </summary>
        public const string SMSUSERNAME = "SMS_UserName";

        /// <summary>
        /// 短信发送key
        /// </summary>
        public const string SMSKEY = "SMS_Key";
        /// <summary>
        /// 图形验证码
        /// </summary>
        public const string CHECKCODE = "CheckCode";
        /// <summary>
        /// 头认证key
        /// </summary>
        public const string AUTHORIZATION = "Authorization";

        /// <summary>
        /// 微信小程序appid
        /// </summary>
        public const string WECHATAPPID = "WechatAppID";

        /// <summary>
        /// 微信小程序AppSecret
        /// </summary>
        public const string WECHATAPPSECRET = "WechatAppSecret";

        /// <summary>
        /// 微信APP appid
        /// </summary>
        public const string WXAPPID = "WxAppID";

        /// <summary>
        /// 微信APP AppSecret
        /// </summary>
        public const string WXAPPSECRET = "WxAppSecret";

        /// <summary>
        /// 微信服务号appid
        /// </summary>
        public const string WECHATFWAPPID = "WechatFWAppID";

        /// <summary>
        /// 微信服务号AppSecret
        /// </summary>
        public const string WECHATFWAPPSECRET = "WechatFWAppSecret";

        /// <summary>
        /// 微信PC appid
        /// </summary>
        public const string WXPCAPPID = "WxPcAppID";

        /// <summary>
        /// 微信PC AppSecret
        /// </summary>
        public const string WXPCAPPSECRET = "WxPcAppSecret";

        /// <summary>
        /// 微信apikey用于签名（apppay支付）
        /// </summary>
        public const string WXAPIKEY = "WxApiKey";

        /// <summary>
        /// 微信apikey用于签名（jspay支付）
        /// </summary>
        public const string WXJSAPIKEY = "WxJsApiKey";

        /// <summary>
        /// 微信apikey用于签名（Nativepay支付）
        /// </summary>
        public const string WXNATIVEAPIKEY = "WxNativeApiKey";

        /// <summary>
        /// app分享标题
        /// </summary>
        public const string SHARETITLE = "ShareTitle";

        /// <summary>
        /// app分享内容
        /// </summary>
        public const string SHARECONTENT = "ShareContent";

        /// <summary>
        /// app分享链接
        /// </summary>
        public const string SHARELINKURL = "ShareLinkUrl";

        /// <summary>
        /// app分享图片
        /// </summary>
        public const string SHAREIMGURL = "ShareImgUrl";

        /// <summary>
        /// 短信验证码
        /// </summary>
        public const string LOGINCODE = "LoginCode";

        /// <summary>
        /// 微信服务号Sessionkey
        /// </summary>
        public const string WXFWOPENID = "WXFWOPENID";

        /// <summary>
        /// 注册协议
        /// </summary>
        public const string REGISTERPORTOCOL = "RegisterProtocol";
        /// <summary>
        /// 注册协议
        /// </summary>
        public const string WCFLAG = "WcFlag";

        /// <summary>
        /// 个推appid
        /// </summary>
        public const string GETUIAPPID = "GeTuiAppId";

        /// <summary>
        /// 个推appkey
        /// </summary>
        public const string GETUIAPPKEY = "GeTuiAppKey";

        /// <summary>
        /// 个推mastersecret
        /// </summary>
        public const string GETUIMASTERSECRET = "GeTuiMasterSecret";
    }
}
