﻿using System;
using Sykj.Infrastructure;

namespace Sykj.Components
{
    /// <summary>
    /// 短信服务
    /// </summary>
    public class ZtsmsSmsServer : ISmsServer
    {
        #region 发送短信

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="number">发送号码</param>
        /// <param name="content">短信内容</param>
        /// <param name="error">返回错误</param>
        /// <returns></returns>
        public bool SendMsg(string number, string content, ref string error)
        {
            try
            {
                string url = "http://www.ztsms.cn/sendNSms.do";
                string userName = BaseConfig.SMSUserName;
                string tkey = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                string other = HashEncrypt.GetMd5(BaseConfig.SMSKey);
                string password = HashEncrypt.GetMd5(other + tkey);

                string data = string.Format("?username={0}&password={1}&mobile={2}&content={3}&tkey={4}&productid=676767&xh=",
                    userName, password, number, content, tkey);

                HttpHelper httpHelper = new HttpHelper(url + data);
                string s = httpHelper.SendGet();
                string code = s.Split(',')[0];
                if (code == "1")
                {
                    return true;
                }
                else
                {
                    error = ExceptionMsg(code);
                    return false;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }
        #endregion

        #region 发送短信异常信息
        /// <summary>
        /// 发送短信异常信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private static string ExceptionMsg(string code)
        {
            string msg = "";
            switch (code)
            {
                case "-1":
                    msg = "用户名或者密码不正确或用户禁用或者是管理账户！";
                    break;
                case "0":
                    msg = "当前用户ID错误,发送短信失败！";
                    break;
                case "2":
                    msg = "余额不够或扣费错误";
                    break;
                case "3":
                    msg = "扣费失败异常（请联系客服）！";
                    break;
                case "6":
                    msg = "有效号码为空！";
                    break;
                case "7":
                    msg = "短信内容为空！";
                    break;
                case "8":
                    msg = "无签名，必须，格式：【签名】";
                    break;
                case "9":
                    msg = "没有Url提交权限";
                    break;
                case "10":
                    msg = "发送号码过多,最多支持2000个号码";
                    break;

                case "11":
                    msg = "产品ID异常或产品禁用";
                    break;
                case "12":
                    msg = "参数异常！";
                    break;
                case "13":
                    msg = "tkey参数错误！";
                    break;
                case "15":
                    msg = "Ip验证失败！";
                    break;
                case "16":
                    msg = "xh参数错误！";
                    break;
                case "19":
                    msg = "短信内容过长，最多支持500个,或提交编码异常导致！";
                    break;
                case "20":
                    msg = "其它错误！";
                    break;
            }
            return msg;
        }
        #endregion
    }
}
